// console.log("Im a pokemon master")

let pokemonTrainer = {
	name: "Karlo Dareb",
	age: 27,
	pokemon: ['pikachu', 'raichu','bulbasaur'],
	
	friends: {
	friendsName1: "Buddy",
	friendsName2: "George",
	friendsName3: "Jorje"
	},

	talk: function(){
		console.log(this.pokemon[0] + " I choose you.")
	}	

}

console.log(pokemonTrainer)
console.log("Result of dot notation")
console.log(pokemonTrainer.name)

console.log("Result of Bracket notation")
console.log(pokemonTrainer["pokemon"])

console.log("Result of talk method")
pokemonTrainer.talk()

function pokemon(name, level, health, attack){

	this.pokemonName = name;
	this.pokemonLevel = level;
	this.pokemonHealth = health;
	this.pokemonAttack = attack;

	//method
	this.tackle = function(targetPokemon){
		console.log(this.pokemonName + " tackles " + targetPokemon.pokemonName)
		let hpAftertackle = targetPokemon.pokemonHealth - this.pokemonAttack;
		console.log(targetPokemon.pokemonName + " health is now reduced to " + hpAftertackle)
}
	//faint method
	 this.faint = function() {
        if(this.pokemonHealth <= 0) {
            console.log(this.pokemonName + " has fainted!");
     
    	}
	}
}




let pikachu = new pokemon("pikachu", 12, 24, 12);
console.log(pikachu)

let geodude = new pokemon("geodude", 8, 16,8);
console.log(geodude)

let mewtoo = new pokemon("mewtoo", 100, 0, 100);
console.log(mewtoo)

pikachu.tackle(geodude)
geodude.tackle(pikachu)

mewtoo.faint()



